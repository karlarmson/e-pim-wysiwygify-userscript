// ==UserScript==
// @name          e-Pim WYSIWYGify
// @namespace     https://gitlab.com/cdr_karl
// @match         https://epim.online/Product/EditProduct*
// @match         https://epim.online/product/EditProduct*
// @match         https://epim.online/product/editproduct*
// @exclude-match https://epim.online/Product/EditProduct*channelCategoryId=*
// @exclude-match https://epim.online/product/EditProduct*channelCategoryId=*
// @exclude-match https://epim.online/product/editproduct*channelCategoryId=*
// @grant         none
// @version       1.0.20
// @author        Karl Armson
// @description   Converts fields on NG15's e-Pim product editing pages from textareas to more user-friendly elements
// @icon          https://gitlab.com/cdr_karl/e-pim-wysiwygify-userscript/-/raw/main/img/ckeditor-logo.png
// @homepageURL   https://gitlab.com/cdr_karl/e-pim-wysiwygify-userscript
// @require       https://cdn.ckeditor.com/ckeditor5/38.0.1/super-build/ckeditor.js
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/dom@2
// @require       https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js
// @resource      GOOGLE_TAXONOMY_TEXT https://www.google.com/basepages/producttype/taxonomy-with-ids.en-GB.txt
// @resource      SELECT2_CSS https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css
// @grant         GM_addStyle
// @grant         GM_getResourceText
// ==/UserScript==

/*
 * Custom CSS
 */

const custom_css = `
  .attributes-quick-add-group {
    border: 1px solid #ddd;
    padding: 10px 10px 0 10px;
    margin-bottom: 10px;
    display: flex;
    flex-wrap: wrap;
  }

  .attributes-quick-add-group h5 {
    display: inline;
    margin-right: 10px;
  }

  .attributes-quick-add-button {
    margin: 0 10px 10px 0;
  }

  .ck-editor__editable_inline {
    min-height: 120px;
    max-height: 200px;
  }`;
GM_addStyle(custom_css);

const select2_css = GM_getResourceText("SELECT2_CSS");
GM_addStyle(select2_css);

/*
 * Main Catalogue tab
 * Inject CKEditor for the Bullets and Body Copy <textarea>s
 */

CKEDITOR.ClassicEditor
  .create(document.getElementById("BulletText"), {
    toolbar: {
      items: [
        {
          label: 'Styles',
          icon: 'text',
          withText: false,
          items: ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript']
        },
        'bulletedList',
        'numberedList',
        'outdent',
        'indent',
        {
          label: 'Insert element',
          icon: 'plus',
          withText: false,
          items: ['link', 'insertTable', 'specialCharacters']
        },
        'sourceEditing'
      ],
      shouldNotGroupWhenFull: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
    link: {
      decorators: {
        addTargetToExternalLinks: {
          mode: 'automatic',
          callback: url => (/^http/.test(url) && !/^(https?:)?\/\/cdn\.ukelectricalsupplies\.com/.test(url) && !/^(https?:)?\/\/www\.ukelectricalsupplies\.com/.test(url) && !/^(https?:)?\/\/cdn\.love4lighting\.co\.uk/.test(url) && !/^(https?:)?\/\/www\.love4lighting\.co\.uk/.test(url)),
          attributes: {
            target: '_blank',
            rel: 'noopener noreferrer'
          }
        },
        defaultProtocol: 'https://'
      }
    },
    removePlugins: [
      'Alignment',
      'AutoImage',
      'Autoformat',
      // 'AutoLink',
      'Autosave',
      'Base64UploadAdapter',
      'ImageBlock',
      'BlockQuote',
      // 'Bold',
      'CKBox',
      'CKFinder',
      'CKFinderUploadAdapter',
      'CloudServices',
      'Code',
      'CodeBlock',
      'Comments',
      'DataFilter',
      'DataSchema',
      'DocumentList',
      'DocumentListProperties',
      'DocumentOutline',
      'EasyImage',
      'ExportPdf',
      'ExportWord',
      'FindAndReplace',
      'FontBackgroundColor',
      'FontColor',
      'FontFamily',
      'FontSize',
      'FormatPainter',
      'GeneralHtmlSupport',
      'Heading',
      'Highlight',
      'HorizontalLine',
      'HtmlComment',
      'HtmlEmbed',
      'Image',
      'ImageCaption',
      'ImageInsert',
      'ImageResize',
      'ImageStyle',
      'ImageToolbar',
      'ImageUpload',
      'ImportWord',
      // 'Indent',
      // 'IndentBlock',
      'ImageInline',
      // 'Italic',
      // 'Link',
      'LinkImage',
      // 'List',
      'ListProperties',
      'Markdown',
      'MathType',
      'MediaEmbed',
      'MediaEmbedToolbar',
      'Mention',
      'PageBreak',
      'Pagination',
      // 'PasteFromOffice',
      'PresenceList',
      'RealTimeCollaborativeComments',
      'RealTimeCollaborativeEditing',
      'RealTimeCollaborativeRevisionHistory',
      'RealTimeCollaborativeTrackChanges',
      'RemoveFormat',
      'RestrictedEditingMode',
      'RevisionHistory',
      // 'SelectAll',
      'SimpleUploadAdapter',
      'SlashCommand',
      // 'SourceEditing',
      // 'SpecialCharacters',
      // 'SpecialCharactersArrows',
      // 'SpecialCharactersCurrency',
      // 'SpecialCharactersEssentials',
      // 'SpecialCharactersLatin',
      // 'SpecialCharactersMathematical',
      // 'SpecialCharactersText',
      'StandardEditingMode',
      // 'Strikethrough',
      'Style',
      // 'Subscript',
      // 'Superscript',
      // 'Table',
      'TableCaption',
      // 'TableCellProperties',
      'TableColumnResize',
      'TableOfContents',
      'TableProperties',
      // 'TableToolbar',
      'Template',
      'TextPartLanguage',
      'TextTransformation',
      'Title',
      'TodoList',
      'TrackChanges',
      'TrackChangesData',
      // 'Underline',
      'EditorWatchdog',
      'WordCount',
      'WProofreader',
    ]
  })
  .then(editor => {
    editor.model.document.on('change:data', (evt, data) => {
      // Update the data in the original <textarea>
      editor.updateSourceElement();
      // Force a change event on the original <textarea>
      document.getElementById("BulletText").dispatchEvent(new Event("change"));
    });
  })
  .catch(error => {
    console.error(error);
  });

CKEDITOR.ClassicEditor
  .create(document.getElementById("BodyCopy"), {
    toolbar: {
      items: [
        {
          label: 'Styles',
          icon: 'text',
          withText: false,
          items: ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript']
        },
        'alignment',
        'bulletedList',
        'numberedList',
        'outdent',
        'indent',
        {
          label: 'Insert element',
          icon: 'plus',
          withText: false,
          items: ['link', 'insertImage', 'insertTable', 'specialCharacters']
        },
        'sourceEditing'
      ],
      shouldNotGroupWhenFull: true
    },
    // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
    link: {
      decorators: {
        addTargetToExternalLinks: {
          mode: 'automatic',
          callback: url => (/^http/.test(url) && !/^(https?:)?\/\/cdn\.ukelectricalsupplies\.com/.test(url) && !/^(https?:)?\/\/www\.ukelectricalsupplies\.com/.test(url) && !/^(https?:)?\/\/cdn\.love4lighting\.co\.uk/.test(url) && !/^(https?:)?\/\/www\.love4lighting\.co\.uk/.test(url)),
          attributes: {
            target: '_blank',
            rel: 'noopener noreferrer'
          }
        },
        defaultProtocol: 'https://'
      }
    },
    removePlugins: [
      // 'Alignment',
      'AutoImage',
      'Autoformat',
      // 'AutoLink',
      'Autosave',
      'Base64UploadAdapter',
      // 'ImageBlock',
      'BlockQuote',
      // 'Bold',
      'CKBox',
      'CKFinder',
      'CKFinderUploadAdapter',
      'CloudServices',
      'Code',
      'CodeBlock',
      'Comments',
      'DataFilter',
      'DataSchema',
      'DocumentList',
      'DocumentListProperties',
      'DocumentOutline',
      'EasyImage',
      'ExportPdf',
      'ExportWord',
      'FindAndReplace',
      'FontBackgroundColor',
      'FontColor',
      'FontFamily',
      'FontSize',
      'FormatPainter',
      'GeneralHtmlSupport',
      'Heading',
      'Highlight',
      'HorizontalLine',
      'HtmlComment',
      'HtmlEmbed',
      // 'Image',
      'ImageCaption',
      // 'ImageInsert',
      // 'ImageResize',
      // 'ImageStyle',
      'ImageToolbar',
      // 'ImageUpload',
      'ImportWord',
      // 'Indent',
      // 'IndentBlock',
      // 'ImageInline',
      // 'Italic',
      // 'Link',
      'LinkImage',
      // 'List',
      'ListProperties',
      'Markdown',
      'MathType',
      'MediaEmbed',
      'MediaEmbedToolbar',
      'Mention',
      'PageBreak',
      'Pagination',
      // 'PasteFromOffice',
      'PresenceList',
      'RealTimeCollaborativeComments',
      'RealTimeCollaborativeEditing',
      'RealTimeCollaborativeRevisionHistory',
      'RealTimeCollaborativeTrackChanges',
      'RemoveFormat',
      'RestrictedEditingMode',
      'RevisionHistory',
      // 'SelectAll',
      'SimpleUploadAdapter',
      'SlashCommand',
      // 'SourceEditing',
      // 'SpecialCharacters',
      // 'SpecialCharactersArrows',
      // 'SpecialCharactersCurrency',
      // 'SpecialCharactersEssentials',
      // 'SpecialCharactersLatin',
      // 'SpecialCharactersMathematical',
      // 'SpecialCharactersText',
      'StandardEditingMode',
      // 'Strikethrough',
      'Style',
      // 'Subscript',
      // 'Superscript',
      // 'Table',
      'TableCaption',
      // 'TableCellProperties',
      'TableColumnResize',
      'TableOfContents',
      'TableProperties',
      // 'TableToolbar',
      'Template',
      'TextPartLanguage',
      'TextTransformation',
      'Title',
      'TodoList',
      'TrackChanges',
      'TrackChangesData',
      // 'Underline',
      'EditorWatchdog',
      'WordCount',
      'WProofreader',
    ]
  })
  .then(editor => {
    // Force the image button to prompt for a URL, and not display the file selection dialogue
    // Select the node that will be observed for mutations
    const targetNode = document.querySelector("#BodyCopy").parentElement.querySelectorAll(".ck-dropdown__panel")[2];
    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(function (mutationList, observer) {
      for (const mutation of mutationList) {
        if (mutation.type === "childList") {
          const imageBtn = document.querySelector('.ck-file-dialog-button');
          if (imageBtn) {
            let imageIcon = imageBtn.querySelector('svg.ck-icon');
            imageIcon.style.width = 'var(--ck-icon-size)';
            const dropdownBtn = document.querySelector('button.ck-splitbutton__arrow');
            let downAngleIcon = dropdownBtn.querySelector('svg.ck-icon');
            imageBtn.style.display = 'none';
            downAngleIcon.remove();
            dropdownBtn.prepend(imageIcon);

            // Image button has been changed, so stop observing
            observer.disconnect();
          }
        }
      }
    });
    // Start observing the target node for configured mutations
    observer.observe(targetNode, {attributes: true, childList: true, subtree: true});

    editor.model.document.on('change:data', (evt, data) => {
      // Update the data in the original <textarea>
      editor.updateSourceElement();
      // Force a change event on the original <textarea>
      document.getElementById("BulletText").dispatchEvent(new Event("change"));
    });
  })
  .catch(error => {
    console.error(error);
  });

/*
 * Categories tab
 * Make fields more user-friendly
 */

// New category selector
let fieldNewCategory = $("#NewWebCategoryId").eq(0);
fieldNewCategory.select2({width: "800px"});

/*
 * Channels tab
 */

// Add link to edit product in other Channel(s)
const otherChannelImplementationsLabel = $("label:contains('Implementations on other Channels:')");
if (otherChannelImplementationsLabel.length > 0 && otherChannelImplementationsLabel.next().is("ul")) {
  const otherChannelImplementationsList = otherChannelImplementationsLabel.next().find("li");
  if (otherChannelImplementationsList.length > 0) {
    otherChannelImplementationsList.each(function(index) {
      if ($(otherChannelImplementationsList[index]).html() !== "None") {
        let manageCategoryLink = $(otherChannelImplementationsList[index]).find("a");
        manageCategoryLink.html("Manage Channel Category <i class=\"fa fa-external-link-alt\"></i>");
        let channelCategoryId = manageCategoryLink.attr("href").match(/\d+$/)[0];

        let productId;
        if (window.location.href.match(/id=(\d+)$/)) {
          productId = window.location.href.match(/id=(\d+)$/)[1];
        } else if (window.location.href.match(/roduct\/(\d+)$/)) {
          productId = window.location.href.match(/roduct\/(\d+)$/)[1];
        }

        manageCategoryLink.after(" &nbsp; &nbsp; <a href=\"https://epim.online/product/EditProduct/" + productId + "?channelCategoryId=" + channelCategoryId + "\" target=\"_blank\">Edit Product in Channel <i class=\"fa fa-external-link-alt\"></i></a>");
      }
    });
  }
}

/*
 * Attributes tab
 * Make fields more user-friendly
 */

function convertTextareaToYesNoSelect(textarea, includeBlank) {
  // Get the current value of the <textarea>
  const currentValue = textarea.val();

  // Get the attributes of the <textarea>, which we will need to add to the new <select>
  let attributes = {};
  $.each(textarea[0].attributes, function (index, attribute) {
    attributes[attribute.nodeName] = attribute.nodeValue;
  });

  // Add the <select> <option>s to the attributes array
  attributes["append"] = [];

  if (includeBlank) {
    if (currentValue === "") {
      attributes["append"].push($("<option />", {value: "", text: "", selected: true}));
    } else {
      attributes["append"].push($("<option />", {value: "", text: ""}));
    }
  }

  if ((!includeBlank && currentValue === "") || currentValue === 0 || currentValue === "0") {
    attributes["append"].push($("<option />", {value: "0", text: "No", selected: true}));
  } else {
    attributes["append"].push($("<option />", {value: "0", text: "No"}));
  }

  if (currentValue === 1 || currentValue === "1") {
    attributes["append"].push($("<option />", {value: "1", text: "Yes", selected: true}));
  } else {
    attributes["append"].push($("<option />", {value: "1", text: "Yes"}));
  }

  textarea.replaceWith($("<select />", attributes));
}

function convertTextareasToYesNoSelect(attributeIds) {
  let textarea;
  $.each(attributeIds, function (index, data) {
    textarea = $(".product-sku-attributes td input[value='" + data.id + "']").parent().find("textarea").eq(0);
    if (textarea.length) {
      convertTextareaToYesNoSelect(textarea, data.includeBlank);
    }
  });
}

const attributeIdsToConvertToYesNoSelect = [
  {id: 44805, includeBlank: false}, // made to order
  {id: 44806, includeBlank: false}, // next day
  {id: 35675, includeBlank: false}, // is bundle
  {id: 5527, includeBlank: false}, // fire rated
  {id: 26161, includeBlank: false}, // bathroom safe
  {id: 26162, includeBlank: false}, // outdoor friendly
  {id: 676, includeBlank: true} // dimmable
]
convertTextareasToYesNoSelect(attributeIdsToConvertToYesNoSelect);

function convertTextareaToInputDate(textarea) {
  // Get the current value of the <textarea>
  const currentValue = textarea.val();

  // Get the attributes of the <textarea>, which we will need to add to the new <input>
  let attributes = {};
  $.each(textarea[0].attributes, function (index, attribute) {
    attributes[attribute.nodeName] = attribute.nodeValue;
  });
  attributes["value"] = currentValue;
  attributes["type"] = "date";

  // Convert the <textarea> to an <input>
  textarea.replaceWith($("<input />", attributes));
}

// Expected date datepicker
const fieldExpectedDate = $(".product-sku-attributes td input[value='44803']").parent().find("textarea").eq(0);
if (fieldExpectedDate.length) {
  convertTextareaToInputDate(fieldExpectedDate);
}

function convertTextareaToSelect(textarea, options) {
  // Get the current value of the <textarea>
  const currentValue = textarea.val();

  // Get the attributes of the <textarea>, which we will need to add to the new <select>
  let attributes = {};
  $.each(textarea[0].attributes, function (index, attribute) {
    attributes[attribute.nodeName] = attribute.nodeValue;
  });

  // Add the <select> <option>s to the attributes array
  attributes["append"] = [];
  $.each(options, function (index, option) {
    if (currentValue === option.value) {
      attributes["append"].push($("<option />", {value: option.value, text: option.text, selected: true}));
    } else {
      attributes["append"].push($("<option />", {value: option.value, text: option.text}));
    }
  });

  textarea.replaceWith($("<select />", attributes));
}

// Google product category selector
let fieldGoogleCat = $(".product-sku-attributes td input[value='44810']").parent().find("textarea").eq(0);
if (fieldGoogleCat.length) {
  let googleCatText = GM_getResourceText("GOOGLE_TAXONOMY_TEXT").split("\n")

  let googleCatOptions = [{value: "", text: ""}];
  $.each(googleCatText, function (index, value) {
    if (!value.startsWith("# Google_Product_Taxonomy_Version") && value.length) {
      value = value.split(" - ");
      googleCatOptions.push({value: value[0], text: value[1]});
    }
  });
  googleCatText = undefined;

  convertTextareaToSelect(fieldGoogleCat, googleCatOptions);
  googleCatOptions = undefined;

  // Update reference to field and initialise Select2
  fieldGoogleCat = $(".product-sku-attributes td input[value='44810']").parent().find("select").eq(0);
  fieldGoogleCat.select2({width: "350px"});
}

// Type selector
let fieldType = $(".product-sku-attributes td input[value='2627']").parent().find("textarea").eq(0);
if (fieldType.length) {
  let typeOptions = [
    {value: "", text: ""},
    {value: "LED_downlight", text: "LED downlight"},
    {value: "LED_lamp", text: "LED lamp"},
    {value: "socket", text: "Socket"}
  ];

  convertTextareaToSelect(fieldType, typeOptions);
}

/*
 * Attributes tab - Change Attributes modal
 * Add buttons to quickly add commonly used fields
 */

function addCustomAttributeField(name) {
  const sortable = $('#sortable');
  const fieldExistsSearch = sortable.find("input[value='" + name + "']");

  if (fieldExistsSearch.length === 0) {
    var newItem = `
      <li class="header-item" style="width:325px; display:inline-block; padding: 10px; line-height: 30px; border: 1px solid #ddd; margin-bottom: 10px;">
        <input type="hidden" value="" name="OriginalIds" />
        <div class="input-group">
          <span class="handle input-group-addon sortable-index" style="cursor:move;">0</span>
          <input class="form-control header-item-input" name="Headers" type="text" value="${name}" />
          <div class="input-group-btn">
            <button class="btn header-item-delete">
              <span class="glyphicon glyphicon-remove"></span>
            </button>
          </div>
        </div>
      </li>`;

    sortable.append(newItem).append(' ');
  }
}

VM.observe(document.body, () => {
  // Find the target node
  const node = document.querySelector('#product-changeheaders-modal');

  if (node) {
    $("#product-changeheaders-modal").on("shown.bs.modal", function (e) {
      const modalBody = $(this).find(".modal-body");

      let divQuickAddButtonsObject = $("<div />", {
        id: "attributes-quick-add",
        append: [
          $("<hr />"),
          $("<h4 />").html("Quick add: ")
            .append($("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "all"
              }).html("Add all custom attributes listed below")
            ),
          $("<div />", {
            class: "attributes-quick-add-group",
            append: [
              $("<h5 />", {
                append: [
                  $("<b />").html("Availability:")
                ]
              }),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Expected Date"
              }).html("Expected Date"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Lead Time"
              }).html("Lead Time"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Made to Order"
              }).html("Made to Order"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Next Day"
              }).html("Next Day")
            ]
          }),
          $("<div />", {
            class: "attributes-quick-add-group",
            append: [
              $("<h5 />", {
                append: [
                  $("<b />").html("Core:")
                ]
              }),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Meta Description (L4L)"
              }).html("Meta Description (L4L)"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Meta Description (UKES)"
              }).html("Meta Description (UKES)"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "YouTube Video ID"
              }).html("YouTube Video ID"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Google Product Category"
              }).html("Google Product Category"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Is Bundle"
              }).html("Is Bundle")
            ]
          }),
          $("<div />", {
            class: "attributes-quick-add-group",
            append: [
              $("<h5 />", {
                append: [
                  $("<b />").html("Classifications:")
                ]
              }),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Collection"
              }).html("Collection"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Product Range"
              }).html("Product Range"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Product Type",
                title: "One of LED downlight, LED lamp, or socket"
              }).html("Product Type")
            ]
          }),
          $("<div />", {
            class: "attributes-quick-add-group",
            append: [
              $("<h5 />", {
                append: [
                  $("<b />").html("Specifications:")
                ]
              }).append(" (if set, will be used instead of corresponding Luckins attribute(s))"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Colour Temperature"
              }).html("Colour Temperature"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Cutout"
              }).html("Cutout"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Dimmable"
              }).html("Dimmable"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Downlight Type"
              }).html("Downlight Type"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Finish"
              }).html("Finish"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Fitting Type"
              }).html("Fitting Type"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Gangs"
              }).html("Gangs"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Insert Colour"
              }).html("Insert Colour"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "IP Rating"
              }).html("IP Rating"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Kelvins"
              }).html("Kelvins"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Life"
              }).html("Life"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Lumens"
              }).html("Lumens"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Module System"
              }).html("Module System"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Socket Type"
              }).html("Socket Type"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Surround Colour"
              }).html("Surround Colour"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Wattage"
              }).html("Wattage"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Weight"
              }).html("Weight")
            ]
          }),
          $("<div />", {
            class: "attributes-quick-add-group",
            append: [
              $("<h5 />", {
                append: [
                  $("<b />").html("Highlights:")
                ]
              }),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Fire Rated"
              }).html("Fire Rated"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Bathroom Safe"
              }).html("Bathroom Safe"),
              $("<button />", {
                class: "btn attributes-quick-add-button",
                type: "button",
                "data-field-name": "Outdoor Friendly"
              }).html("Outdoor Friendly")
            ]
          })
        ]
      });

      const modalDivQuickAddButtons = modalBody.find("#attributes-quick-add").eq(0);
      if (modalDivQuickAddButtons.length) {
        modalDivQuickAddButtons.replaceWith(divQuickAddButtonsObject);
      } else {
        modalBody.append(divQuickAddButtonsObject);
      }
    })
  }
});

$("body").delegate(".attributes-quick-add-button", "click", function (e) {
  e.preventDefault();

  $(this).prop("disabled", true);

  const fieldName = $(this).data("field-name");

  if (fieldName === "all") {
    const customAttributes = [
      "Expected Date",
      "Lead Time",
      "Made to Order",
      "Next Day",
      "Meta Description (L4L)",
      "Meta Description (UKES)",
      "YouTube Video ID",
      "Google Product Category",
      "Is Bundle",
      "Collection",
      "Product Range",
      "Product Type",
      "Colour Temperature",
      "Cutout",
      "Dimmable",
      "Downlight Type",
      "Finish",
      "Fitting Type",
      "Gangs",
      "Insert Colour",
      "IP Rating",
      "Kelvins",
      "Life",
      "Lumens",
      "Module System",
      "Socket Type",
      "Surround Colour",
      "Wattage",
      "Weight",
      "Fire Rated",
      "Bathroom Safe",
      "Outdoor Friendly"
    ];

    for (const index in customAttributes) {
      addCustomAttributeField(customAttributes[index]);
    }
  } else {
    addCustomAttributeField(fieldName);
  }

  reindexItems();
});
