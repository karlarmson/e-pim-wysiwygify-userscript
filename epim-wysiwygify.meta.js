// ==UserScript==
// @name          e-Pim WYSIWYGify
// @namespace     https://gitlab.com/cdr_karl
// @match         https://epim.online/Product/EditProduct*
// @match         https://epim.online/product/EditProduct*
// @match         https://epim.online/product/editproduct*
// @exclude-match https://epim.online/Product/EditProduct*channelCategoryId=*
// @exclude-match https://epim.online/product/EditProduct*channelCategoryId=*
// @exclude-match https://epim.online/product/editproduct*channelCategoryId=*
// @grant         none
// @version       1.0.20
// @author        Karl Armson
// @description   Converts fields on NG15's e-Pim product editing pages from textareas to more user-friendly elements
// @icon          https://gitlab.com/cdr_karl/e-pim-wysiwygify-userscript/-/raw/main/img/ckeditor-logo.png
// @homepageURL   https://gitlab.com/cdr_karl/e-pim-wysiwygify-userscript
// @require       https://cdn.ckeditor.com/ckeditor5/38.0.1/super-build/ckeditor.js
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/dom@2
// @require       https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js
// @resource      GOOGLE_TAXONOMY_TEXT https://www.google.com/basepages/producttype/taxonomy-with-ids.en-GB.txt
// @resource      SELECT2_CSS https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css
// @grant         GM_addStyle
// @grant         GM_getResourceText
// ==/UserScript==
